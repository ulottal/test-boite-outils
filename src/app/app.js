import '../vdm/boite-outils.css'
import '../vdm/vdm.css'

import './style.css'

import view from './view'

window.state = {
  search: '',
  listing: [],
  planets: {}
}

const render = state => {
  window.state = state
  document.body.innerHTML = view(state)

  document.getElementById('search-form').onsubmit = ev => {
    ev.preventDefault()
    render({
      ...state,
      search: ev.target.search.value,
      listing: Object.keys(state.planets).filter(planetName => state.planets[planetName].name.includes(ev.target.search.value))
    })
  }

  console.log(state)
}


// Initial render
render(window.state)




// const pipe = (...fns) => x => fns.reduce((v, f) => f(v), x);


const getAllPlanets = () => Promise.all([1, 2, 3, 4, 5, 6, 7].map(page => fetch('https://swapi.co/api/planets/?page=' + page)))
  .then(responses => Promise.all(responses.map(res => res.json())))
  .then(pagesData => pagesData.reduce((data, page) => data.concat(page.results), []))




const byNames = planets => planets.reduce((byName, planet) => {
  byName[planet.name] = planet
  return byName
}, {})

    

// const sortByClimate = planets => planets.reduce((climates, planet) => {
//   planet.climate.split(', ').forEach(climate => {
//     climates[climate] = {
//       ...climates[climate],
//       [planet.name]: planet
//     }
//   });
//   return climates
// }, {})













getAllPlanets()
  .then(planets => render({
    ...state, 
    listing: planets.map(planet => planet.name),
    planets: byNames(planets)
  }))

