const view = state => (/*html*/`
  <header class="has-top-banner" role="banner">
    <div class="top-banner ">
      <div class="top-banner-container">
        <span class="top-banner-description">Version bêta du site de la Ville de Montréal</span>
        <a class="top-banner-link" href="#" target="_blank" aria-label="En savoir plus">En savoir +</a>
      </div>
      <a class="back-link" href="#" target="_blank" aria-label="Quitter la version bêta du site Lien externe">Quitter la version bêta du site</a>
    </div>  <nav class="navbar navbar-light navbar-expand justify-content-between" role="navigation">
      <div class="d-flex w-100">
        <button class="navbar-button navbar-button-lg menu-toggler slide-menu-control" type="button" aria-haspopup="true" aria-controls="navbarSideMenu" data-target="slide-menu-left" data-action="toggle">
          <span class="vdm vdm-106-menu-simple" aria-hidden="true"></span>
          <span class="button-label d-none d-md-flex">Menu</span>
        </button>      <button class="navbar-button navbar-button-lg navbar-search-toggler d-none d-lg-inline-flex" id="navbarDropdownSearch" aria-haspopup="true" aria-expanded="false">
          <span class="sr-only">Ouvrir la barre de recherche</span>
          <span class="vdm vdm-002-recherche " aria-hidden="true"></span>
          <span class="button-label">Recherche</span>
        </button>
        <div class="slide-menu" id="slide-menu-left" style="left: 0px; right: auto; transform: translateX(-100%); display: block;">
          <div class="slide-menu-header">
            <button class="btn-close js-button-close">
              <span class="vdm vdm-046-grand-x"></span>
              <span class="button-label d-none d-lg-flex">Fermer</span>
            </button>
          </div>
        
          <div class="slide-menu-search js-nav-search d-block d-lg-none" id="slide-menu-search">
            <form>
              <div class="form-group has-clear">
                <div class="input-group-icon input-group-icon-left">
                  <button class="btn-close-search js-button-close-search d-none">
                    <span class="vdm vdm-065-fleche-gauche"></span>
                    <span class="sr-only">Quitter la recherche</span>
                  </button>
                  <span class="vdm vdm-002-recherche " aria-hidden="true"></span>
                  <input type="search" class="form-control" id="text-icon-left" placeholder="Rechercher un lieu ou un service" autocomplete="off">
                  <button class="btn-clear hidden d-none">
                    <span class="vdm vdm-071-mini-x-circulaire form-control-clear hidden" aria-hidden="true"></span>
                  </button>
                </div>
              </div>
            </form>
          </div>
        
          <div class="slider"><ul class="side-menu">
            <li>
              <a href="../../patterns/03-templates-02-collectrices-00-homepage/03-templates-02-collectrices-00-homepage.html">Accueil</a>
            </li>
            <li class="has-submenu">
              <a href="#">Nouvelles<span class="vdm vdm-063-fleche-droite" aria-hidden="true"></span></a>
              <ul class="submenu"><li><a href="" class="slide-menu-control back-link" data-action="back"><span class="vdm vdm-058-chevron-gauche" aria-hidden="true"></span> Retour</a></li>
                <li>
                  <a href="../../patterns/03-templates-02-collectrices-05-collectrice-alerts/03-templates-02-collectrices-05-collectrice-alerts.html">Avis et alertes</a>
                </li>
                <li>
                  <a href="">Actualités</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="">Demandes et paiements</a>
            </li>
            <li>
              <a href="../../patterns/04-pages-02-collectrices-01-informations-pratiques/04-pages-02-collectrices-01-informations-pratiques.html">Informations pratiques</a>
            </li>
            <li class="has-submenu">
              <a href="#">Vie municipale<span class="vdm vdm-063-fleche-droite" aria-hidden="true"></span></a>
              <ul class="submenu"><li><a href="" class="slide-menu-control back-link" data-action="back"><span class="vdm vdm-058-chevron-gauche" aria-hidden="true"></span> Retour</a></li>
                <li>
                  <a href="#">Fonctionnement</a>
                </li>
                <li>
                  <a href="../../patterns/04-pages-02-collectrices-02-tous-les-elus/04-pages-02-collectrices-02-tous-les-elus.html">Mairesse et élus</a>
                </li>
                <li class="has-submenu">
                  <a href="#">Conseils décisionnels et comité exécutif<span class="vdm vdm-063-fleche-droite" aria-hidden="true"></span></a>
                  <ul class="submenu"><li><a href="" class="slide-menu-control back-link" data-action="back"><span class="vdm vdm-058-chevron-gauche" aria-hidden="true"></span> Retour</a></li>
                    <li>
                      <a href="../../patterns/04-pages-04-instances-01-conseil-municipal/04-pages-04-instances-01-conseil-municipal.html">Conseil municipal</a>
                    </li>
                    <li>
                      <a href="../../patterns/03-templates-06-instance-et-commission-04-instance/03-templates-06-instance-et-commission-04-instance.html">Conseil d'agglomération</a>
                    </li>
                    <li>
                      <a href="../../patterns/03-templates-06-instance-et-commission-04-instance-variante-comite-executif/03-templates-06-instance-et-commission-04-instance-variante-comite-executif.html">Comité exécutif</a>
                    </li>
                    <li>
                      <a href="../../patterns/04-pages-02-collectrices-08-conseils-arrondissement/04-pages-02-collectrices-08-conseils-arrondissement.html">Conseils d'arrondissement</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="#">Commissions permanentes</a>
                </li>
                <li>
                  <a href="#">Reddition de comptes</a>
                </li>
                <li>
                  <a href="#">Organisation municipale</a>
                </li>
                <li>
                  <a href="../../patterns/04-pages-02-collectrices-05-participation-citoyenne/04-pages-02-collectrices-05-participation-citoyenne.html">Participation citoyenne</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="../../patterns/03-templates-02-collectrices-06-collectrice-lieux/03-templates-02-collectrices-06-collectrice-lieux.html">Trouver un lieu</a>
            </li>
            <li>
              <a href="#">Nous joindre</a>
            </li>
            <li>
              <a href="#">English</a>
            </li>
          </ul></div>
        </div>
      </div>
      <a class="navbar-brand mx-auto" href="../../patterns/03-templates-02-collectrices-00-homepage/03-templates-02-collectrices-00-homepage.html">
        <img src="https://services.accept.montreal.ca/boite-outils/images/logos/vdm-logo.svg" class="brand-logo" alt="Logo - Ville de Montréal">
      </a>
      <div class="col-right d-flex w-100">
        <div class="d-flex align-items-center ml-auto">
          <button class="navbar-button button-underline d-none d-lg-inline-flex">
            <span class="button-label">Créer mon compte</span>
          </button>
          <button class="navbar-button btn-login d-inline-flex">
            <span class="vdm vdm-003-profil " aria-hidden="true"></span>
            <span class="button-label d-none d-md-inline-flex">Me connecter</span>
            <span class="sr-only">Me connecter</span>
          </button>
        </div>
      </div>
      <div class="overlay"></div>
    </nav>
    <div id="navbar-search" class="navbar-search">
      <div class="container">
        <div class="inner-container">
          <button class="navbar-button js-button-close">
            <span class="vdm vdm-065-fleche-gauche"></span>
            <span class="sr-only">Quitter la recherche</span>
          </button>
          <div class="form-group has-clear">
            
            <div class="input-group-icon">
              <input type="text" class="form-control" id="inputtextfieldid" placeholder="Rechercher une information ou un service">
              <button class="btn-clear d-none">
                <span class="vdm vdm-071-mini-x-circulaire form-control-clear hidden" aria-hidden="true"></span>
                <span class="sr-only">Clear search input</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>



  <main class="main-content " id="main-content" aria-label="Main Content">

    <header class="content-header content-header-hub content-header-overlay " style="background-image: url(https://picsum.photos/3888/2592?image=553)">
      <div class="container-fluid container-fluid-xl container-fluid container-fluid-xl">
        <div class="row">
          <div class="col-12">
            <h1 class="h4">SWAPI test app</h1>
            <h2 class="display-1">The planets of Star Wars</h2>
          </div>
          <div class="col-12 col-lg-8">
            <form method="post" id="search-form">
              <div class="form-group">
                <div class="input-group-icon input-group-icon-lg input-group-icon-left">
                  <span class="vdm vdm-002-recherche " aria-hidden="true"></span>
                  <input name="search" type="text" class="form-control form-control-lg form-control-dark" id="input-group-search" value="${state.search}" placeholder="Rechercher">
                </div>
              </div>
            </form>
          </div>
          <div class="col-12">
            <div class="quick-links">
              <span class="quick-links-label">Recherche rapide : </span>
              <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Quick link #1</a></li>
                <li class="list-inline-item"><a href="#">Quick link #2</a></li>
                <li class="list-inline-item"><a href="#">Quick link #3</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section id="nav-filters" class="section-filters">
      <div class="container">
        <!-- Nav Modal on mobile, becomes static nav with dropdowns on tablets and up -->
        <div class="nav-modal modal modal-left-pane fade" id="navModalFilters-1" aria-labelledby="navFiltersModalLabel">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header d-flex d-md-none">
                <h5 class="modal-title" id="navFiltersModalLabel">Filtres de recherche</h5>
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Recherche</button>
              </div>
              <div class="modal-body">
                <ul class="nav nav-filters">
                  <li class="nav-item dropdown dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" data-display="static">Filter #1</a>
                    <div class="dropdown-menu ">
                      <form>
                        <div class="dropdown-item form-group">
                          <div class="custom-control custom-radio  ">
                            <input type="radio" id="rdiofilter-1" name="radiosGroup" class="custom-control-input" value="rdiofilter-1">
                            <label class="custom-control-label" for="rdiofilter-1">Radio</label>
                          </div>
                        </div>
                      </form>
                    </div>
                  </li>
                  <li class="nav-item dropdown dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" data-display="static">Filter #2</a>
                    <div class="dropdown-menu ">
                      <form>
                        <div class="dropdown-item form-group">
                          <div class="custom-control custom-checkbox ">
                            <input type="checkbox" class="custom-control-input" id="chckboxfilter-1" value="chckboxfilter-1">
                            <label class="custom-control-label" for="chckboxfilter-1">Checkbox</label>
                          </div>
                        </div>
                        <div class="dropdown-item form-group">
                          <div class="custom-control custom-checkbox ">
                            <input type="checkbox" class="custom-control-input" id="chckboxfilter-2" value="chckboxfilter-2">
                            <label class="custom-control-label" for="chckboxfilter-2">Checkbox option #2</label>
                          </div>
                        </div>
                      </form>
                    </div>
                  </li>
                </ul>
    
    
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="container">
      <div class="row">
        <div class="col-12 d-block d-md-none pt-4">
          <button type="button" class="btn btn-secondary btn-block" data-toggle="modal" data-target="#navModalFilters-1" data-backdrop="false">
            Filtres de recherche
          </button>
        </div>
      </div>
    </div>
    


    <section class="page-section">
      <div class="container">
        <div class="hub-section-heading">
          <div class="row">
            <div class="col-12 col-lg">
              <h2 class="list-group-hub-heading">List group heading</h2>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="list-group list-group-hub">


              ${
                state.listing.length > 0
                ? state.listing.map(planetName => {
                  const planet = state.planets[planetName]
                  return (/*html*/`
                    <a class="list-group-item list-group-item-action " href="#">
                      <div class="list-group-item-body">
                        <div class="list-group-max-width">
                          <span class="list-group-item-title">${planet.name}</span>
                            <div class="list-group-item-content ">
                              <span class="">Climate</span>
                              <span class="text-dark separator-brand">${planet.climate}</span>
                            </div>
                        </div>
                      </div>
                      <div class="list-group-item-footer">
                        <span class="vdm vdm-063-fleche-droite " aria-hidden="true"></span>
                      </div>
                    </a>
                  `)
                }).join('')
                : 'Loading...'
              }
                


            </div>
          </div>
        </div>

      </div>
    </section>


    
  </main>
`)




export default view